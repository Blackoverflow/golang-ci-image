# Golang CI image
This project provides an docker image for building and testing golang code in gitlab-ci.

It is based on the docker image from gitlab.com/pantomath-io/demo-tools.

It consists of
* The official golang image (version 1.19.1-buster)
* staticcheck for static code analysis (version 2022.1.3)
* gocover-cobertura for coverage reports in xml format
* clang-11 for memory sanitization with the -msan flag of go

## Usage
Use it like this in your .gitlab.ci.yml

    image: registry.gitlab.com/blackoverflow/golang-ci-image:latest
