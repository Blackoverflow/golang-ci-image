# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Released]

## [1.0.0] - 2022-09-10
### Added
- Docker Image with golang, staticcheck and clang-11 [@Blackoverflow](https://gitlab.com/Blackoverflow).
- License (MIT)
- Readme
- Gitlab CI for building and pushing the Docker Image onto https://registry.gitlab.com
- Changelog

## [1.0.1] - 2022-09-10
#### Changed
- Fix export of environment variable CC=clang-11 [@Blackoverflow](https://gitlab.com/Blackoverflow).

## [1.1.0] - 2022-09-15
### Added
- Add gocover-covertura to image for coverage reports in xml format [@Blackoverflow](https://gitlab.com/Blackoverflow)
#### Changed
- fix Docker tagging in pipeline [@Blackoverflow](https://gitlab.com/Blackoverflow).
- run pipeline jobs in stage 'build_and_deploy'
