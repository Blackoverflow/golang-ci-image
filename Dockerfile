# Base image: https://hub.docker.com/_/golang/
FROM golang:1.19.1-buster
MAINTAINER Thorsten Ploß <thorsten.ploss@mailbox.org>

# Install staticcheck
ENV GOPATH /go
ENV PATH ${GOPATH}/bin:$PATH
RUN go install honnef.co/go/tools/cmd/staticcheck@2022.1.3

# Install gocover-cobertura
RUN go install github.com/boumenot/gocover-cobertura@v1.2.0

# Install clang-11
RUN apt-get update -qq && apt-get install -y  -qq --no-install-recommends \
    clang-11 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Set Clang as default CC
ENV CC clang-11
